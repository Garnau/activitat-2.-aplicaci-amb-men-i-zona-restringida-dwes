<?php
    session_start();

    $usuaris = array(
      "admin" => "admin",
    );

    include_once("formulari_login.php");
    $uSER = $_POST['UserName'];
    $PASS = $_POST['Password'];
    $CORRECTE =  isset($_POST['UserName'])
              && isset($_POST['Password'])
              && isset($usuaris[$uSER])
              && $usuaris[$uSER] == $PASS;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title> PROCESSA LOGIN </title>
</head>
<body>
<div style="margin: 30px 10%;">
  <?php
    if($CORRECTE){
      $_SESSION['Usuari'] = $uSER;
      header("menu.php");
    } else {
      setcookie("ErrorCookie", "Log Error");
      header("formulari_login.php");
    }
  ?>
</div>
</body>
</html>
