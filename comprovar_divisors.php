<?php
  session_start();
  if(!isset($_SESSION["admin"])){
    die("<a href='formulari_login.php'>");
  }else{
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>COMPROVAR DIVISORS</title>
  </head>
  <body>
    <div style="margin: 30px 10%;">
      <form action="comprovar_divisors.php" method="post" id="myform" name="myform">
        <label>Insereix un número:</label>
        <input type="numbdr" value="" size="30" maxlength="100" name="n" id="" /><br /><br />
        <button id="mysubmit" type="submit">Envia</button><br /><br />
      </form>
      <?php

      $num = $_POST["n"];
      $divi = "El número " . $num . " no és un número primer i els seus divisors són ";
      $cont=0;
      $prim = false;

      // Funcion que recorre todos los numero desde el 2 hasta el valor recibido
      for($i=2;$i<=$num;$i++)
      {
        $prim = true;
        if($num%$i==0)
          $divi = $divi . ", " . $i;
      }

      if($prim === true) {
        echo $prim
      } else {
        echo "El número " . $num . " és un número primer.";
      }
      ?>
    </div>
    <a href="menu.php"> MENÚ </a>
  </body>
</html>
<?php
  }
?>
