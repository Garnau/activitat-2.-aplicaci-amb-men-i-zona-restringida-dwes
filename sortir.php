<?php
  //inicia sessió
  session_start();
  //elimina variables
  session_unset();
  //elimina info registrada
  session_destroy();
  //borra la cookie
  setcookie(session_name(),'',0,'/');
?>
