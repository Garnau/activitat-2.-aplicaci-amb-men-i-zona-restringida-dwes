<?php
  session_start();
  if(!isset($_SESSION["admin"])){
    die("<a href='formulari_login.php'>");
  }else{

    $num = $_POST["n"];
    $cadena = "";
    $ite = 0;
    $max = 0;

    while($num >= 1) {
      if(primer($num)) {
        $num = $num / 2;
      } else {
        $num = (3 * $num) + 1;
      }
      $ite++;
      if($num > $max) {
        $max = $num;
      }

    }

    function primer($num)
    {
      $cont=0;
      // Funcion que recorre todos los numero desde el 2 hasta el valor recibido
      for($i=2;$i<=$num;$i++)
      {
        if($num%$i==0)
        {
          # Si se puede dividir por algun numero mas de una vez, no es primo
          if(++$cont>1)
            return false;
        }
      }
    return true;
    }

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>CONJETURA COLLATZ</title>
  </head>
  <body>
    <div style="margin: 30px 10%;">
      <form action="comprovar_divisors.php" method="post" id="myform" name="myform">
        <label>Insereix un número:</label>
        <input type="numbdr" value="" size="30" maxlength="100" name="n" id="" /><br /><br />
        <button id="mysubmit" type="submit">Envia</button><br /><br />
      </form>
      <?php

      echo "La seqüència del " . $num . " és: ";
      echo "{" . $cadena . "} desprñes de " . $ite . " iteracions i arribant a un màxim de " . $max . ".";

      ?>
    </div>
    <a href="menu.php"> MENÚ </a>
  </body>
</html>
<?php
}
?>
